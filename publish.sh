#/bin/bash
echo "Last version was: $(git describe --tags --abbrev=0)"
echo "What shoud this version be?"
version=$(gum input --placeholder "e.g 1.0.0")
cd ..
./archive.sh
zip -r slamsdk-package/slamsdk.xcframework.zip ./build/slamsdk.xcframework
cd ./slamsdk-package
# Compute the new checksum
new_checksum=$(swift package compute-checksum ./slamsdk.xcframework.zip)

# Update the checksum in Package.swift
sed -i '' -e "s/[a-f0-9]\{64\}/$new_checksum/g" Package.swift
sed -i '' -E "s|(https://gitlab.com/combain/slamsdk-ios/-/raw/)[^/]+|\1$version|" Package.swift
git add .
git commit -m "Release version $version"
git tag -a $version -m "Release version $version"
git push origin main --tags
echo "Done!"
