# SlamSDK iOS

## About

This is a SDK wrapper around the Combain SlamAPI. See https://gitlab.com/combain/CPSSDK#overview for more info.

## Installing

Add the following package to your project:

`https://gitlab.com/combain/slamsdk-ios`

## Using

First initialize and start the SDK like this

```swift
let apiKey = "YOUR API KEY"
CPSSDK.setup(apiKey: apiKey)
CPSSDK.instance?.startSlam()
```


Create an instance of CPSSDKDelegate like this

```swift
class LocationDelegate : CPSSDKDelegate {
 func positionUpdate(details: PositioningDetails) {
        print("------------------")
        print("Positioning update: \(details.toString)")
        print("------------------")
    }
}
````

Then subscribe to location updates like this

```swift
var sdk = CPSSDK.instance;
sdk?.requestLocationUpdates(callback: LocationDelegate())
```
