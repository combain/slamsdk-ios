// swift-tools-version: 5.10
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "slamsdk",
    platforms: [
        .iOS(.v14)
    ],
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        .library(
            name: "slamsdk",
            targets: ["slamsdk"]),
    ],
    targets: [
        .binaryTarget(
            name: "slamsdk",
            url: "https://gitlab.com/combain/slamsdk-ios/-/raw/1.0.6/slamsdk.xcframework.zip?ref_type=tags&inline=false",
            checksum: "2675b94b4858e6b924fb33038163ecc0dd4f4cc98d4e81783d35d1a5f3ddd019"
        )
    ]
)
